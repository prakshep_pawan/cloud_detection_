from django.apps import AppConfig


class ResamplingConfig(AppConfig):
    name = 'resampling'
