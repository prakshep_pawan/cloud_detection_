import gdal as gd
from s2cloudless import S2PixelCloudDetector
import numpy as np
import os
from osgeo import gdal as gd
import gdalconst
import matplotlib.pyplot as plt
from mttkinter import mtTkinter as tk
def cloudmask(path):
    bd = {'B02': 1, 'B03': 1, 'B04': 1, 'B08': 1, 'B05': 2, 'B06': 2, 'B07': 2, 'B8A': 2, 'B11': 2, 'B12': 2, "B01": 6,
          'B09': 6, 'B10': 6}
    # In[8]:
    kd = ['B02', 'B03', 'B04', 'B08', 'B05', 'B06', 'B07', 'B8A', 'B11', 'B12', 'B01', 'B09', 'B10']
    # In[9]:
    # Resampling Bands to 10m
    # ds = []
    for band in kd:
        i = bd[band]
        add = path + str(band) + '.tiff'
        if os.path.isfile(add) == False:
            continue
            print(error)
        input_ds = gd.Open(add, gd.GA_ReadOnly)
        inputProj = input_ds.GetProjection()
        inputTrans = input_ds.GetGeoTransform()
        inputTransnew = (inputTrans[0], float(inputTrans[1] / i), inputTrans[2], inputTrans[3], inputTrans[4],
                         float(inputTrans[5] / i))  # Divide as per the required resolution
        bandreference = input_ds.GetRasterBand(1)
        x = input_ds.RasterXSize * i  # Multiply as per the required resolution
        y = input_ds.RasterYSize * i
        try:
            os.mkdir(path + 'resample1')
        except:
            pass
        outputfile = path + "resample1/" + str(band) + '.tiff'
        driver = gd.GetDriverByName('GTiff')
        output = driver.Create(outputfile, x, y, 1, bandreference.DataType)
        output.SetGeoTransform(inputTransnew)
        output.SetProjection(inputProj)

        gd.ReprojectImage(input_ds, output, inputProj, inputProj, gdalconst.GRIORA_Bilinear)
        # ds.append(output.ReadAsArray(0,0,basic_band.RasterXSize,basic_band.RasterYSize))

        del output

    Band = ["B01", "B02", "B03", "B04", "B05", "B06", "B07", "B08", "B8A", "B09", "B10", "B11", "B12"]
    bands = []
    for k in Band:
        ds = gd.Open(path + "resample1/" + str(k) + '.tiff')
        basic_band = gd.Open(path + "resample1/B01.tiff")  # B01 acts as reference for all bands
        band1 = ds.GetRasterBand(1)
        # b1= band.ReadAsArray()
        k = band1.ReadAsArray(0, 0, basic_band.RasterXSize, basic_band.RasterYSize)
        bands.append(k)
        print(k.shape)
    # In[11]
    bands[0].max()
    # In[12]:
    # List temp stores all the normalized bands
    temp = []
    for band in bands:
        #     print(band.min())
        #     print (band.max())
        x = (band - band.min()) / (band.max() - band.min())  # normalization function
        temp.append(np.expand_dims(x, axis=2))
    # In[13]:
    temp
    # In[14]:
    # x is merged 3D array
    x = np.concatenate([temp[0], temp[1], temp[3], temp[4], temp[7], temp[8], temp[9], temp[10], temp[11], temp[12]],
                       axis=2)
    # In[15]:
    # Increase dimension of x by 1 to convert 3-D to 4-D array
    f_array = np.expand_dims(x, axis=0)

    # f_array is final array to be used further
    # In[16]:
    f_array
    cloud_detector = S2PixelCloudDetector(threshold=0.4, average_over=2, dilation_size=2)
    # In[24]:
    cloud_probs = cloud_detector.get_cloud_probability_maps(f_array)

    # In[25]:

    cloud_masks = cloud_detector.get_cloud_masks(f_array)
    return (cloud_masks[0])
def plot_cloud_mask(mask, figsize=(15, 15)):
    path = "/Users/pawan/Desktop/43PGQ/2018-2-23/"
    """
    Utility function for plotting a binary cloud mask.
    """
    # plt.figure(figsize=figsize)
    plot = plt.subplot(1, 1, 1)
    plot.imshow(mask, cmap=plt.cm.gray)
    plt.savefig( "tree/static/img/cloudmask.png")









