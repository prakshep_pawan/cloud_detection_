import geopandas as gp
from .json import *
import matplotlib.pyplot as plt
import numpy as np

import tree.sentinaldata as sd
from .analysis import *
from .visual import *


def download_data(path_to_shp):
    df = sd.read_shp(path_to_shp)
    polygons = sd.list_of_plots(df)
    download_data(polygons)
    coordinates = sd.get_coordinates(polygons)
    return coordinates


def analyse_data(path_to_data):
    path_to_plot, plots_list = plots_(path_to_data)
    plot_wise_indices = {}
    for i in range(len(plots_list)):
        path_to_dates = path_to_plot + plots_list[i] + '/'
        dates_list = ordered_dates_by_path(path_to_dates)

        dates_indices = {}
        if (len(dates_list) != 0):
            for j in range(len(dates_list)):
                dates_indices[dates_list[j]] = get_indices(dates_list[j], path_to_dates)
            plot_wise_indices[plots_list[i]] = dates_indices

    return plot_wise_indices








def gaussian_analysis(path_to_data, path_to_shp, label):
    plot_wise_indices = analyse_data(path_to_data)
    final_crop, crop_average, crop_std, zscore, mse = final_visualize(plot_wise_indices, label)
    return final_crop, crop_average, crop_std, zscore, mse


def make_geojsons(label, path_to_data):
    geojson(label, path_to_data)
