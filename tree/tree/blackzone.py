
import numpy as np
import os
from osgeo import gdal as gd
from osgeo import ogr, osr
from gdalconst import GA_ReadOnly
from osgeo import gdalconst
import matplotlib.pyplot as plt







from s2cloudless import S2PixelCloudDetector
from s2cloudless import CloudMaskRequest
import numpy as np
import pandas as pd

def blackZone(plotId,farmerId,dates,Th):



    Band = ["B01" ,"B02" ,"B04" ,"B05" ,"B08" ,"B8A" ,"B09" ,"B10" ,"B11" ,"B12"]   
    assetId = str(plotId) + '_' + str(farmerId) + '_' + 'S'
    k= np.zeros((114, 145))
    print(k.shape)
    kk = []
    count = 0
    for i in range(len(dates)):
        lst = []
        for band in Band:
            ds = gd.Open('/tmp/tamp1/' + assetId + '/' + str(dates[i][0]) + '_' + str(dates[i][1]) + '_' + str(
            dates[i][2]) + '/' + str(band) + '.jp2')
            band_array = ds.GetRasterBand(1).ReadAsArray()
        #         band_norm =(band_array - band_array.min())/(band_array.max()- band_array.min())
            lst.append(np.expand_dims(band_array, axis=2))

    ### Concat on axis=2
        y = np.concatenate((lst[0], lst[1], lst[2], lst[3], lst[4], lst[5], lst[6], lst[7], lst[8], lst[9]), axis=2)
        f_array = np.expand_dims(y, axis=0)
        f_array = f_array.astype(np.int8)
    #     print (f_array)

        cloud_detector = S2PixelCloudDetector(threshold=0.6, average_over=4, dilation_size=2)
        cloud_probs = cloud_detector.get_cloud_probability_maps(f_array)
        cloud_masks = cloud_detector.get_cloud_masks(f_array)
        if k.shape !=  cloud_masks.shape:
       	   k  = np.zeros(shape = cloud_masks[0].shape)
        t = cloud_masks[0]
        print(k.shape)
        k = k + t
        a = sum(
        x.count(1) for x in cloud_masks[0].tolist())  # a is number of cloudy pixels. b is number of noncloudy pixels
        if (a != 0):
           count = count + 1
        b = sum(x.count(0) for x in cloud_masks[0].tolist())
        kk.append((a / (a + b)) * 100)  # It gives percentage of cloudy pixels
    #     print (cloud_masks[0])
    #     plot_probability_map(cloud_probs[0])
    #     plot_cloud_mask(cloud_masks[0],figsize=(15,15))
    #     print (cloud_probs[0])

    ### Save to Cloud Probability Tiff
        ds_cloud = gd.Open('/tmp/tamp1/' + assetId + '/' + str(dates[i][0]) + '_' + str(dates[i][1]) + '_' + str(
            dates[i][2]) + '/' + 'B02.jp2')
        y1 = cloud_probs[0].shape[1]
        x1 = cloud_probs[0].shape[0]

        geotiff = gd.GetDriverByName('GTiff')
        options = ['PROFILE=GeoTIFF']
        output = geotiff.Create('/tmp/tamp1/' + assetId + '/' + str(dates[i][0]) + '_' + str(dates[i][1]) + '_' + str(
            dates[i][2]) + '/' + 'Cloud_Prob.tiff', y1, x1, 1, gd.GDT_Float32, options=options)
        output.SetGeoTransform(ds_cloud.GetGeoTransform())

        output.GetRasterBand(1).WriteArray(cloud_probs[0].reshape(x1, y1))

        output.SetGeoTransform(ds_cloud.GetGeoTransform())
        wkt_sci = ds_cloud.GetProjection()

    # setting spatial reference of output raster
        srs = osr.SpatialReference()
        srs.ImportFromWkt(wkt_sci)
        output.SetProjection(srs.ExportToWkt())

        output.FlushCache()
        output = None

    s = k / count
    return s

    k11=np.zeros((114, 145))
    for i in range(a1):
        for j in range(a2):
            k11[i][j]=s[i][j]






    for i in range(a1):
        for j in range(a2):
            if(k11[i][j]<=th):
                k11[i][j]=0
        else:
            k11[i][j]=1
    return k11
