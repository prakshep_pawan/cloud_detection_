
from django.http import JsonResponse,HttpResponse
from rest_framework.views import APIView
from rest_framework.response import Response
from django.shortcuts import render
from django.contrib.auth.models import User
import json
from django.shortcuts import render_to_response
from django.views.decorators.csrf import csrf_exempt
# from .download import *

from .final import *
from .json import *
import gdal
import geojson as gj
from .url_download import *
from .blackzone import *
from .sumit import *
@csrf_exempt
def post(request,format='none'):
    if request.method == 'POST':
       k = json.loads(request.body)
       print(k)
       lat = list(map(float, k['lat'].strip('[]').split(',')))
       lon = list(map(float, k['lon'].strip('[]').split(',')))
       sowing_date = k['date']
       plotId = k['plotId']
       farmerId = k['farmerId']
       dates = soil_veg_data(plotId, farmerId, sowing_date, lat, lon)
       mask = probtif(plotId,farmerId,dates)
       final_geojson(mask[0], plotId, farmerId, dates,i=0)
       kp=create_url(plotId, farmerId, lat, lon,dates,i=0)
       return HttpResponse(kp)

@csrf_exempt
def get(request, format='none'):
    if request.method == 'POST':
        k = json.loads(request.body)
        print(k)
        lat = list(map(float, k['lat'].strip('[]').split(',')))
        lon = list(map(float, k['lon'].strip('[]').split(',')))
        sowing_date = k['date']
        harvest_date = k['harv']
        plotId = k['plotId']
        farmerId = k['farmerId']
        dates = soil_veg_datasa(plotId, farmerId, sowing_date, harvest_date, lat, lon)
        mask = blackZone(plotId,farmerId,dates,Th=0.5)
        final_geojson(mask, plotId, farmerId,dates,i=0)
        kp=create_url(plotId, farmerId, lat, lon, dates,i=0)

        return HttpResponse(kp)








