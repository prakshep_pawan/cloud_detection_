import os
import gdal as gd
import numpy as np
from datetime import datetime as dt
import math


def plots_(path_):
    return path_, sorted(os.listdir(path_))


def str_to_date(item):
    return str(dt.strptime(item, '%Y_%m_%d').date())


def bands(date):
    return sorted(glob.glob(date + '/*'))


def plot_dates_data(path_to_dates):
    dates_list = os.listdir(path_to_dates)
    dates_of_plot = []
    for i in range(len(dates_list)):
        if not dates_list[i].endswith("json"):
            dates_of_plot.append(dates_list[i])
    return dates_of_plot


def time_order_dates(date_list):
    date_ = []
    for i in date_list:
        date_.append(str_to_date(i))
    return sorted(date_)


# In[412]:


def remove_zero(array):
    ds = array
    ds[np.isnan(ds)] = -9999
    return ds


def band_to_array(band):
    return band.ReadAsArray()


def new_date(date):
    dates = date.split('-')
    if dates[1][0] == '0':
        if dates[2][0] == '0':
            date = dates[0] + '_' + dates[1][1] + '_' + dates[2][1]
        else:
            date = dates[0] + '_' + dates[1][1] + '_' + dates[2]
    else:
        if dates[2][0] == '0':
            date = dates[0] + '_' + dates[1] + '_' + dates[2][1]
        else:
            date = dates[0] + '_' + dates[1] + '_' + dates[2]
    return date


def extract_bands(date, plot_path):
    bands = {}
    keys = ['B03', 'B04', 'B05_res', 'B06_res', 'B08']
    for i in range(len(keys)):
        path = plot_path + date + '/' + keys[i] + '.jp2'
        bands[keys[i]] = gd.Open(path)
    return bands


def band_to_indices(bands, normalise=False):
    np.seterr(invalid='ignore')
    keys = ['B02', 'B03', 'B04', 'B05_res', 'B06_res', 'B08']
    b3 = band_to_array(bands[keys[1]])
    b4 = band_to_array(bands[keys[2]])
    b5 = band_to_array(bands[keys[3]])
    b6 = band_to_array(bands[keys[4]])
    b8 = band_to_array(bands[keys[5]])

    NDVI = (b8 - b4) / (b8 + b4)

    NDWI = (b8 - b3) / (b8 + b3)

    EBBI = 0.1 * (b5 - b4) / (np.sqrt(b5 + b6))

    NDVI, NDWI, EBBI = list(map(remove_zero, [NDVI, NDWI, EBBI]))

    indices = {
        'NDVI': NDVI,
        'NDWI': NDWI,
        'EBBI': EBBI,
    }

    return indices


def get_indices(date, path_plot):
    return band_to_indices(extract_bands(date, path_plot))


def minimum_dates_number(plot_wise_indices):
    plot_keys = list(plot_wise_indices.keys())
    mini = math.inf
    for i in range(len(plot_keys)):
        mini = min(mini, len(list(plot_wise_indices[plot_keys[i]].keys())))
    return mini


def ordered_dates(plot_wise_indices, plot_key):
    date_listes = list(plot_wise_indices[plot_key].keys())
    listes = time_order_dates(date_listes)
    new_listes = []
    for i in range(len(listes)):
        new_listes.append(new_date(listes[i]))
    return new_listes


def final_database(plot_wise_indices):
    mini = minimum_dates_number(plot_wise_indices)
    plot_keys = list(plot_wise_indices.keys())
    Datelist = []

    for i in range(mini):
        DATE = {}
        for j in range(len(plot_keys)):
            new_dates = ordered_dates(plot_wise_indices, plot_keys[j])
            DATE[plot_keys[j]] = plot_wise_indices[plot_keys[j]][new_dates[i]]
        Datelist.append(DATE)
    return Datelist


