from .analysis import *
from tree.sentinaldata import *
import numpy as np
import matplotlib.pyplot as plt
from sklearn.metrics import mean_squared_error
import numpy.ma as ma


def crop_analysis(plot_wise_indices):
    avg = []

    datelist = an.final_database(plot_wise_indices)

    no_of_dates = an.minimum_dates_number(plot_wise_indices)
    plot_indices = list(plot_wise_indices.keys())

    for date in datelist:
        for i in range(len(plot_indices)):
            avg.append(np.mean(date[plot_indices[i]]['NDVI']))

    n = np.array(avg).reshape(no_of_dates, len(plot_indices))
    final_crop = np.transpose(n)
    ymask = np.logical_or(final_crop > 1, final_crop < -1)
    final_crop = ma.MaskedArray(final_crop, ymask)

    # outliers = np.array(np.where(final_crop[:,0] < -1))[0]
    # final_crop = np.delete(np.copy(final_crop), outliers, axis=0)
    # outliers = np.array(np.where(final_crop[:,0] > 1))[0]
    # final_crop = np.delete(np.copy(final_crop), outliers, axis=0)

    crop_average = ma.mean(final_crop, axis=0)
    crop_std = ma.std(final_crop, axis=0)

    return (final_crop, crop_average, crop_std)


def mse(final_crop, crop_average, plot_wise_indices):
    mse = []
    plot_indices = list(plot_wise_indices.keys())
    for i in range(len(plot_indices)):
        mse.append(mean_squared_error(final_crop[i], crop_average))

    return mse


def visualise(final_crop, crop_average, plot_wise_indices):
    plot_indices = list(plot_wise_indices.keys())
    for i in range(len(plot_indices)):
        plt.plot(final_crop[i])
    plt.plot(crop_average, linewidth='5')


def z_score(final_crop, crop_average, crop_std):
    return (final_crop - crop_average) / (crop_std)