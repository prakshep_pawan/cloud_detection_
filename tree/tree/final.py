
import os
from osgeo import gdal as gd
from osgeo import ogr, osr
from gdalconst import GA_ReadOnly
from osgeo import gdalconst
import matplotlib.pyplot as plt
from s2cloudless import S2PixelCloudDetector
from s2cloudless import CloudMaskRequest
import numpy as np

def probtif(plotId,farmerId,dates):
    i=0
    Band = ["B01", "B02", "B04", "B05", "B08", "B8A", "B09", "B10", "B11", "B12"]
    lst = []
    assetId = str(plotId) + '_' + str(farmerId) + '_' + 'S'
    for band in Band:
        ds = gd.Open('/tmp/tamp1/' + assetId + '/' + str(dates[i][0]) + '_' + str(dates[i][1]) + '_' + str(dates[i][2]) + '/' + str(band) + '.jp2')
        band_array = ds.GetRasterBand(1).ReadAsArray()
        lst.append(np.expand_dims(band_array, axis=2))

    ### Concat on axis=2
    y = np.concatenate((lst[0], lst[1], lst[2], lst[3], lst[4], lst[5], lst[6], lst[7], lst[8], lst[9]), axis=2)
    f_array = np.expand_dims(y, axis=0)
    f_array = f_array.astype(np.int8)
    #     print (f_array)
    cloud_detector = S2PixelCloudDetector(threshold=0.4, average_over=4, dilation_size=2)
    cloud_probs = cloud_detector.get_cloud_probability_maps(f_array)
    cloud_masks = cloud_detector.get_cloud_masks(f_array)

    # print(cloud_masks[0])
    # plot_probability_map(cloud_probs[0])
    # plot_cloud_mask(cloud_masks[0], figsize=(15, 15))


    # ### Save to Cloud Probability Tiff
    ds_cloud = gd.Open('/tmp/tamp1/' + assetId + '/' + str(dates[i][0]) + '_' + str(dates[i][1]) + '_' + str(dates[i][2]) + '/' + 'B02' + '.jp2')
    y1 = cloud_probs[0].shape[1]
    x1 = cloud_probs[0].shape[0]
    geotiff = gd.GetDriverByName('GTiff')
    options = ['PROFILE=GeoTIFF']
    output = geotiff.Create('/tmp/tamp1/'+assetId+'/'+str(dates[i][0])+'_'+str(dates[i][1])+'_'+str(dates[i][2])+'/Cloud_Prob.tiff', y1, x1, 1, gd.GDT_Float32, options=options)
    output.SetGeoTransform(ds_cloud.GetGeoTransform())
    output.GetRasterBand(1).WriteArray(cloud_probs[0].reshape(x1, y1))

    output.SetGeoTransform(ds_cloud.GetGeoTransform())
    wkt_sci = ds_cloud.GetProjection()
    #
    # # setting spatial reference of output raster
    srs = osr.SpatialReference()
    srs.ImportFromWkt(wkt_sci)
    output.SetProjection(srs.ExportToWkt())
  
    output.FlushCache()
    output = None
    return cloud_masks
