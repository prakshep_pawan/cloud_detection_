
# stop celery confictions
from __future__ import absolute_import, unicode_literals
# Common
import os
import shutil
import json
import urllib3
from collections import Counter
from datetime import datetime as dt
import time
import datetime

# For Multiprocessing
import multiprocessing as mp
from multiprocessing import Process, Manager

# For Geospatial data processing
from osgeo import ogr, osr
from osgeo import gdal as gd
from gdalconst import GA_ReadOnly
from osgeo import gdalconst

# For connection with S3
import boto3
import botocore
from io import BytesIO

# For numeric data processingå
from scipy.stats import zscore
import pandas as pd
import numpy as np

# For geojson files
import pygeoj as pj

# Ignore warings
import warnings
from urllib3 import PoolManager
import numpy
import geopandas as gp

warnings.filterwarnings('ignore')

def path_name(MGRS, year, month, day, sequence, band):
    # The function returns aws address for the required band of the respective scene : http://sentinel-pds.s3-website.eu-central-1.amazonaws.com/
    # Military Grid Reference System (MGRS) : https://en.wikipedia.org/wiki/Military_Grid_Reference_System
    # year : the respective year
    # month : the respective month
    # day : the respective day
    # sequence : e.g. 0 - in most cases there will be only one image per day. In case there are more (in northern latitudes), the following images will be 1,2,…\

    utm = MGRS[:2]  # grid zone designator
    lat_band = MGRS[2:3]  # latitude band are lettered C- X (omitting the letters "I" and "O")
    square = MGRS[3:]  # e.g DG - pair of letters designating one of the 100,000-meter side grid squares inside the grid zone
    address= "http://sentinel-s2-l1c.s3.amazonaws.com/tiles/"
    final_address = address + utm + "/" + str(lat_band) + "/" + str(
        square) + "/" + str(year) + "/" + str(month) + "/" + str(day) + "/" + str(sequence) + "/" + str(band) + ".jp2"
    return final_address

def join(row):
    row['acquisitionDate'] = str(row['year']) + "-" + str(row["month"]) + "-" + str(row["day"])
    return row

def return_dates(sowing_date, df):
    print (df['acquisitionDate'])
    dk = df[(df['acquisitionDate']) > numpy.array(sowing_date, dtype='datetime64')]

    # Store all the values for year, month, day and sequence
    dates = []

    d = dk.index[0]
    year = dk['year'][d]
    month = dk['month'][d]
    day = dk['day'][d]
    sequence = dk['sequence'][d]
    dates.append([year, month, day, sequence])

    if len(dates) == 0:
        sowing_date = monthdelta(sowing_date, -1)
        return return_dates(sowing_date, df)
    return dates, sowing_date

def monthdelta(date, delta):
    #     date = datetime.date(date.year, date.month)
    #     date = date.date()
    m, y = (date.month + delta) % 12, date.year + ((date.month) + delta - 1) // 12
    if not m: m = 12
    d = min(date.day, [31,
                       29 if y % 4 == 0 and not y % 400 == 0 else 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31][m - 1])
    return date.replace(day=d, month=m, year=y)

def make_directory(assetId):
    directory = '/tmp/tamp1/' + assetId
    if os.path.exists(directory):
        shutil.rmtree(directory)
    os.makedirs(directory)

def avg_lat_lon(lat ,lon):
    latitude = np.sum(lat) / float(len(lat))
    longitude = np.sum(lon) / float(len(lon))
    return latitude , longitude

def get_place_name(path ,label ,latitude ,longitude):
    dic = {'STATE': "NAME_1" ,'DISTRICT': "NAME_2", 'TALUKA': "NAME_3"}
    label = dic[label]
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = ogr.Open(path)
    layer = dataSource.GetLayer()

    pt = ogr.Geometry(ogr.wkbPoint)
    pt.SetPoint_2D(0, longitude, latitude)
    layer.SetSpatialFilter(pt)

    for feature in layer:
        place_name = feature.GetField(label)

    return place_name


def fetch_MGRS(path, latitude, longitude):
    driver = ogr.GetDriverByName("GeoJSON")
    dataSource = ogr.Open(path)
    layer = dataSource.GetLayer()

    pt = ogr.Geometry(ogr.wkbPoint)
    pt.SetPoint_2D(0, longitude, latitude)
    layer.SetSpatialFilter(pt)

    for feature in layer:
        MGRS = feature.GetField("Name")

    return MGRS


def plot_extents(lat, lon, assetId):
    cc = []
    for i in range(len(lat)):
        cc.append([lon[i], lat[i]])
    coordinates = []
    coordinates.append(cc)

    nfs = pj.new()
    nfs.add_feature(properties={"SUM": str(5050)}, geometry={"type": "Polygon", "coordinates": coordinates})
    nfs.add_all_bboxes()
    nfs.update_bbox()
    nfs.save('/tmp/tamp1/' + assetId + '/' + "NFS.geojson")

    source_ds = ogr.Open('/tmp/tamp1/' + assetId + '/' + "NFS.geojson")
    source_layer = source_ds.GetLayer()
    a, b, c, d = source_layer.GetExtent()
    return a, b, c, d


def make_directories(assetId, dates):
    for i in range(len(dates)):
        print('directory_' + str(dates[i][0]) + '-' + str(dates[i][1]) + '-' + str(dates[i][2]) + "_formed...")
        dc = '/tmp/tamp1/' + assetId + '/' + str(dates[i][0]) + '_' + str(dates[i][1]) + '_' + str(
            dates[i][2])
        if os.path.exists(dc):
            shutil.rmtree(dc)
        os.makedirs(dc)


def fetch_bands_data(MGRS, BANDS, dates, assetId, a, b, c, d, ):
    for i in range(len(dates)):
        print("data for " + str(i) + " date starts.............................................")
        for band in BANDS:
            print(band + " data downloading")
            local_path = path_name(str(MGRS), str(dates[i][0]), str(dates[i][1]), str(dates[i][2]), str(dates[i][3]),
                                   str(band))

            http = PoolManager()
            r = http.request('GET', local_path)

            if r.status == 404: continue
            address = "/" + str('vsicurl') + "/" + local_path
            ds = gd.Open(address, GA_ReadOnly)
            ulx, uly, lrx, lry = a, d, b, c
            ds = gd.Translate(
                '/tmp/tamp1/' + assetId + '/' + str(dates[i][0]) + '_' + str(dates[i][1]) + '_' + str(
                    dates[i][2]) + '/' + str(band) + '.jp2', ds, projWin=[ulx, uly, lrx, lry],
                projWinSRS="+proj=longlat +datum=WGS84 +no_defs")
            ds = None
        print("data for " + str(i) + " date downloaded_________________________________________")


def resampling(Bands, dates, assetId):
    for i in range(len(dates)):
        for band in Bands:
            print("resampling data of " + band)
            inputfile = '/tmp/tamp1/' + assetId + '/' + str(dates[i][0]) + '_' + str(
                dates[i][1]) + '_' + str(
                dates[i][2]) + '/' + str(band) + ".jp2"
            if os.path.isfile(inputfile) == False: continue
            input_ds = gd.Open(inputfile, GA_ReadOnly)
            inputProj = input_ds.GetProjection()
            inputTrans = input_ds.GetGeoTransform()

            referencefile = '/tmp/tamp1/' + assetId + '/' + str(dates[i][0]) + '_' + str(
                dates[i][1]) + '_' + str(
                dates[i][2]) + '/' + "B04.jp2"
            reference = gd.Open(referencefile, GA_ReadOnly)
            referenceProj = reference.GetProjection()
            referenceTrans = reference.GetGeoTransform()
            bandreference = reference.GetRasterBand(1)
            x = reference.RasterXSize
            y = reference.RasterYSize

            outputfile = '/tmp/tamp1/' + assetId + '/' + str(dates[i][0]) + '_' + str(
                dates[i][1]) + '_' + str(
                dates[i][2]) + '/' + str(band) + ".jp2"
            driver = gd.GetDriverByName('GTiff')
            output = driver.Create(outputfile, x, y, 1, bandreference.DataType)
            output.SetGeoTransform(referenceTrans)
            output.SetProjection(referenceProj)

            gd.ReprojectImage(input_ds, output, inputProj, referenceProj, gdalconst.GRA_NearestNeighbour)

            del output
            print("resampling of " + band + "done")


def soil_veg_data(plotId, farmerId, sowing_date, lat, lon):
    assetId = str(plotId) + '_' + str(farmerId) + '_' + 'S'

    # Create a folder with name assetId where data will be stored temporary for Sentinel-2

    make_directory(assetId)
    print("DIRECTORY FOR " + str(plotId) + " formed")
    print("Downloading data for " + str(plotId))
    ## AVG LATITUDE LONGITUDE
    latitude, longitude = avg_lat_lon(lat, lon)

    # Download whole geojson from bucket
    client = boto3.client('s3',
                          aws_access_key_id='AKIAILOL763GKUG7LEYQ',
                          aws_secret_access_key='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')

    resource = boto3.resource('s3')
    bucket = resource.Bucket('prakshep-sentinel')

    obj = client.get_object(Bucket='prakshep-sentinel', Key='Sentinel/sentinel_index.csv')
    df = pd.read_csv(BytesIO(obj['Body'].read()))

    client.download_file(Bucket='prakshep-sentinel', Key='Sentinel/Features.geojson',
                         Filename='/tmp/tamp1/' + assetId + '/' + 'Features.geojson')

    # FETCH STATE NAME, DISTRICT NAME, TALUK NAME FROM WHERE REQUEST IS MADE
    state_path = "./IND_SHP/IND_adm1.shp"
    district_path = "./IND_SHP/IND_adm2.shp"
    taluka_path = "./IND_SHP/IND_adm3.shp"

    state_name = get_place_name(state_path, "STATE", latitude, longitude)
    district_name = get_place_name(district_path, "DISTRICT", latitude, longitude)
    taluka_name = get_place_name(taluka_path, "TALUKA", latitude, longitude)

    ## CREATE FOLDER FOR THE USER IF DOESN'T EXIST
    pathCheck = state_name + '/' + district_name + '/' + taluka_name + '/' 'Users' + '/' + str(
        farmerId) + '/' + 'Plot' + '/' + str(plotId) + '/'

    s3 = boto3.resource('s3',
                        aws_access_key_id='AKIAILOL763GKUG7LEYQ',
                        aws_secret_access_key='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')

    try:
        s3.Object('prakshepindia', pathCheck).load()
    except botocore.exceptions.ClientError as e:
        if e.response['Error']['Code'] == "404":
            client.put_object(Bucket='prakshepindia', Key=pathCheck)
        else:
            # Something else has gone wrong.
            raise
    else:
        client.put_object(Bucket='prakshepindia', Key=pathCheck)

    # Fetch MGRS of the scene

    geojson_path = '/tmp/tamp1/' + assetId + '/' + 'Features.geojson'
    MGRS = fetch_MGRS(geojson_path, latitude, longitude)

    # finding dates for downloading data
    df = df[df['data_coverage'] >= 90.0]
    df = df[(df.utm_zone == int(MGRS[:2])) & (df.lat_band == MGRS[2:3]) & (df.square == MGRS[3:])]
    df = df.apply(join, axis=1)
    df['acquisitionDate'] = pd.to_datetime(df['acquisitionDate'])
    sowing_date = dt.strptime(sowing_date, "%Y-%m-%d").date()
    dates, sw_date = return_dates(sowing_date, df)

    # Required bands for downloading
    Bands_10 = ['B02', 'B03', 'B04', 'B08']  # 10m resolution
    Bands_20 = ['B05', 'B06', 'B07', 'B8A', 'B11', 'B12']  # 20m resolution
    Bands_60 = ['B01', 'B09', 'B10']  # 60m resolution

    # Bands needed to be resampled
    Bands = ['B05', 'B06', 'B07', 'B8A', 'B11', 'B12', 'B01', 'B09', 'B10']  # 10m downsampling
    #     bands = ['B03', 'B04', 'B08', 'B05', 'B06']  # 5m or 1m downsampling

    ## CONFIGURE GDAL WITH AWS REGION WHERE SENTINEL-2 DATA IS STORED AND THE FILE FORMAT
    gd.SetConfigOption('AWS_REGION', 'eu-central-1')
    gd.SetConfigOption('CPL_VSIL_CURL_ALLOWED_EXTENSIONS', '.jp2')
    gd.SetConfigOption('VSI_CACHE', 'TRUE')
    gd.SetConfigOption('GDAL_DISABLE_READDIR_ON_OPEN', 'TRUE')

    ## GET SPATIAL EXTENT OF THE PLOT
    a, b, c, d = plot_extents(lat, lon, assetId)

    ## MAIN LOOP SENTINEL-2 (It downloads the required bands in assetId folder under tmp and resample them to 10m, 5m, 1m)
    # form directory for dates to store data
    make_directories(assetId, dates)
    print("Downloading Starts")
    fetch_bands_data(MGRS, Bands_10, dates, assetId, a, b, c, d)
    print(
        "_____________...............________________Bands_10 data downloaded_____________...............________________")
    fetch_bands_data(MGRS, Bands_20, dates, assetId, a, b, c, d)
    print(
        "_____________...............________________Bands_20 data downloaded_____________...............________________")
    fetch_bands_data(MGRS, Bands_60, dates, assetId, a, b, c, d)
    print(
        "_____________...............________________Bands_60 data downloaded_____________...............________________")

    print("/////Resampling of data starts////")
    resampling(Bands, dates, assetId)
    print("/////Resampling of data ends////")

    return dates















