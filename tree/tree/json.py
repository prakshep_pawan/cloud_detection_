
from branca.colormap import linear
import branca.colormap as cm
import gdal as gd
import numpy as np
import os
import gdal as gd, osr, ogr
import pygeoj as pj

from subprocess import call
import subprocess

import tree.analysis as an
import tree.execution as ex
import tree.visual as vs
import tree.sentinaldata as sd
import boto3


def processing_of_array(label_values, score):
    final_pixel_values = np.zeros((label_values.shape[0], label_values.shape[1]))
    for i in range(label_values.shape[0]):
        for j in range(label_values.shape[1]):
            if score[i][j] < -2:
                final_pixel_values[i][j] = 0
            elif -2 < score[i][j] < -1:
                final_pixel_values[i][j] = 1
            else:
                final_pixel_values[i][j] = 2
    return final_pixel_values


def colormap(index_array):
    index_array = index_array.flatten()
    mini = int(index_array.min())
    maxa = int(index_array.max())

    colormap = cm.LinearColormap(colors=['black','white'], vmin=mini, vmax=maxa)



    hex_code = []
    for i in range(len(index_array)):
        hex_code.append(colormap.rgb_hex_str(index_array[i]))
    return hex_code


def make_geojson(path_to_geojson, hex_code):
    n_true = pj.new()
    tf = pj.load(path_to_geojson)
    print(len(hex_code))
    for index, item in enumerate(tf):
        n_true.add_feature(properties={"fill": hex_code[index], "stroke": '#00FFFFFF', "strokeOpacity": '0'},
                           geometry={"type": "Polygon", "coordinates": item.geometry.coordinates})
    os.remove('/tmp/temp.tiff')
    os.remove('/tmp/temp1.tiff')
    os.remove('/tmp/temp_json.json')
    return n_true


def convert_to_TIF(path_to_new_tiff, a, path_to_old_tiff):
    band = gd.Open(path_to_old_tiff + 'Cloud_Prob.tiff')
    print( band,type(band)) 
    print(band.ReadAsArray())
    index_arr = a
    geotiff = gd.GetDriverByName('GTiff')
    output = geotiff.CreateCopy(path_to_new_tiff + 'temp' + '.tiff', band, 0)
    output = geotiff.Create(path_to_new_tiff + 'temp' + '.tiff', band.RasterXSize, band.RasterYSize, 1, gd.GDT_Int32)
    output.GetRasterBand(1).SetNoDataValue(0.0)
    output.GetRasterBand(1).WriteArray(index_arr)
    output.SetGeoTransform(band.GetGeoTransform())
    wkt_sci = band.GetProjection()
    # setting spatial reference of output raster
    srs = osr.SpatialReference()
    srs.ImportFromWkt(wkt_sci)
    output.SetProjection(srs.ExportToWkt())
    output = None


def utm_lat_lon(path_to_new_tiff):
    lists = path_to_new_tiff.split('/')
    path_to_final_tiff = '/tmp/temp1.tiff'
    command = str(
        'gdalwarp -t_srs ' + "'" + '+proj=longlat +datum=WGS84 +no_defs' + "' " + path_to_new_tiff + " " + path_to_final_tiff)
    call(command, shell=True)


def blank_geojson(array, path_to_old_tiff):
    new_array = np.random.choice(range(1, array.shape[0] * array.shape[1] + 1), array.shape[0] * array.shape[1],
                                 replace=False).reshape(array.shape[0], array.shape[1])
    path_to_new_tiff = '/tmp/'
    convert_to_TIF(path_to_new_tiff, new_array, path_to_old_tiff)
    utm_lat_lon(path_to_new_tiff + 'temp.tiff')
    path_to_blank_geojson = '/tmp/temp_json.json'
    command = str('gdal_polygonize.py ' + '/tmp/temp1.tiff' + ' -f "GeoJSON" ' + path_to_blank_geojson)
    call(command, shell=True)

    return path_to_blank_geojson


def final_geojson(final_pixel_values,plotId,farmerId,dates,i):
    assetId =  str(plotId)+'_'+str(farmerId)+'_'+'S'
    path_to_old_tiff =  '/tmp/tamp1/'+assetId+'/'+str(dates[i][0])+'_' + str(dates[i][1]) + '_' + str(dates[i][2]) + '/'
    path_to_blank_geojson = blank_geojson(final_pixel_values, path_to_old_tiff)
    hex_code = colormap(final_pixel_values)
    temp_geojson = make_geojson(path_to_blank_geojson, hex_code)
    assetId = str(plotId) + '_' + str(farmerId) + '_' + 'S'
    path = '/tmp/'+assetId+'/'
    if not os.path.exists(path):
        os.makedirs(path)
    path_json = path + 'jsons/'
    if not os.path.exists(path_json):
        os.makedirs(path_json)
    temp_geojson.save(path_json +str(dates[i][0])+'_' + str(dates[i][1]) + '_' + str(dates[i][2]) + '.json' )

    return temp_geojson

def avg_lat_lon(lat,lon):
    latitude = np.sum(lat) / float(len(lat))
    longitude = np.sum(lon) / float(len(lon))
    return latitude , longitude

def get_place_name(path, label, lat, lon):
    latitude,longitude  = avg_lat_lon(lat,lon)
    dic = {'STATE': "NAME_1", 'DISTRICT': "NAME_2", 'TALUKA': "NAME_3"}
    label = dic[label]
    driver = ogr.GetDriverByName("ESRI Shapefile")
    dataSource = ogr.Open(path)
    layer = dataSource.GetLayer()

    pt = ogr.Geometry(ogr.wkbPoint)
    pt.SetPoint_2D(0, longitude, latitude)
    layer.SetSpatialFilter(pt)

    for feature in layer:
        place_name = feature.GetField(label)

    return place_name


def create_url(plotId, farmerId, lat, lon, dates,i):
    state_path = "./IND_SHP/IND_adm1.shp"
    district_path = "./IND_SHP/IND_adm2.shp"
    taluka_path = "./IND_SHP/IND_adm3.shp"

    state_name = get_place_name(state_path, "STATE", lat, lon)
    district_name = get_place_name(district_path, "DISTRICT", lat, lon)
    taluka_name = get_place_name(taluka_path, "TALUKA", lat, lon)

    assetId = str(plotId) + '_' + str(farmerId) + '_' + 'S'

    client = boto3.client('s3',
                          aws_access_key_id='AKIAILOL763GKUG7LEYQ',
                          aws_secret_access_key='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')

    milliSeconds = 1000 * 60 * 5
    path = state_name + '/' + district_name + '/' + taluka_name + '/' + 'Users' + '/' + str(
        farmerId) + '/' + assetId + '_'+str(dates[i][0])+'_' + str(dates[i][1]) + '_' + str(dates[i][2])  + ".geojson"

    params_ = {'Bucket': 'prakshepindia',
               'Key': path}

    input_path = '/tmp/' + assetId + '/jsons/'  + str(dates[i][0]) + '_' + str(dates[i][1]) + '_' + str(dates[i][2]) + '.json'
    client.upload_file(input_path, 'prakshepindia', path)

    url_ = client.generate_presigned_url('get_object', params_, milliSeconds)

    return url_

def make_jsons_directory(new_path_to_final_geojson, i):
    lists = new_path_to_final_geojson.split('/')

    path_to_json_dir = '/' + lists[1] + '/' + lists[2] + '/'
    if not os.path.exists(path_to_json_dir):
        os.makedirs(path_to_json_dir)

    path_to_dates = path_to_json_dir + str(i) + '/'
    if not os.path.exists(path_to_dates):
        os.makedirs(path_to_dates)

    path_to_plots_jsons = path_to_dates + lists[4] + '/'
    if not os.path.exists(path_to_plots_jsons):
        os.makedirs(path_to_plots_jsons)


def geojson(label, path_to_data):
    plot_wise_indices = ex.analyse_data(path_to_data)
    plot_names = list(plot_wise_indices.keys())
    datelist = an.final_database(plot_wise_indices)

    final_crop, crop_average, crop_std, zscore, mse = ex.final_visualize(plot_wise_indices, label)

    for i in range(len(datelist)):
        for plot in plot_names:
            new_dates = an.ordered_dates(plot_wise_indices, plot)
            path_to_old_tiff = path_to_data + plot + '/' + new_dates[i] + '/'
            label_values = datelist[i][plot][label]
            crop_average1, crop_std1 = crop_average[i], crop_std[i]
            score = (label_values - crop_average1) / crop_std1
            final_pixel_values = processing_of_array(label_values, score)

            geojson = final_geojson(final_pixel_values, path_to_old_tiff)

            new_path_to_final_geojson = '/india/jsons/' + str(i) + '/' + plot + '/' + label + '.json'
            make_jsons_directory(new_path_to_final_geojson, i)

            geojson.save(new_path_to_final_geojson)
            os.remove('/tmp/temp.tiff')
            os.remove('/tmp/temp1.tiff')
            os.remove('/tmp/temp_json.json')
